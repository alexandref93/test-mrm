import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import FormUser from './index';

test('snapshot formUser', () => {

  const wrapper = mount(
    withIntl(
      <FormUser />
    ),
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('snapshot formUser with initial value', () => {

  const wrapper = mount(
    withIntl(
      <FormUser user={{firstName: 'Carlos Alexandre', lastName: 'Fuechter', age: '20-29'}} />
    ),
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});
