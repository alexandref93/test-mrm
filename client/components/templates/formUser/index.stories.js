import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import FormUser from './index';

storiesOf('UI/Templates/FormUser', module)
	.add('default', () => <FormUser />)
	.add('with initial user', () => <FormUser user={{firstName: 'Carlos Alexandre', lastName: 'Fuechter', age: '20-29'}} />);
