// @flow
import { useState, useEffect } from 'react';
import { injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';
import Router from 'next/router';

import Button from '../../atoms/button';
import Section from '../../atoms/section';
import InputPhoto from '../../molecules/inputPhoto';
import Form from '../../organisms/form';
import { isEmailValid } from '../../../utils/validation';

import { validateInfo } from './utils';
import s from './index.mod.css';

type Props = {
  intl: IntlShape,
  user: Object,
};

const confForm = {
	fields: [{
		label: 'templates.formUser.name',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'firstName',
			placeholder: 'templates.formUser.name.firstName.placeholder',
			isValid: (value) => /^\D{0,20}$/g.test(value),
		}, {
			type: 'text',
			name: 'lastName',
			placeholder: 'templates.formUser.name.lastName.placeholder',
			isValid: (value) => /^\D+$/g.test(value),
		}],
	}, {
		label: 'templates.formUser.age',
		isMandatory: true,
		inputs: [{
			type: 'range',
			name: 'age',
			steps: [{
				label: '13-19',
				value: '13-19',
			}, {
				label: '20-29',
				value: '20-29',
			}, {
				label: '30-45',
				value: '30-45',
			}, {
				label: '45+',
				value: '45+',
			}],
		}],
	}, {
		label: 'templates.formUser.email',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'email',
			placeholder: 'templates.formUser.email.placeholder',
			isValid: isEmailValid,
		}],
	}, {
		label: 'templates.formUser.telephone',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'telephone',
			placeholder: 'templates.formUser.telephone.placeholder',
		}],

	}, {
		label: 'templates.formUser.country',
		isMandatory: true,
		inputs: [{
			type: 'dropdown',
			name: 'country',
			placeholder: 'templates.formUser.country.placeholder',
			options: {
				BR: 'Brasil',
				CA: 'Canadá',
				PT: 'Portugal',
				EN: 'Inglaterra',
			},
		}],
	}, {
		label: 'templates.formUser.state',
		isMandatory: true,
		inputs: [{
			type: 'dropdown',
			name: 'state',
			placeholder: 'templates.formUser.state.placeholder',
			options: {
				PR: 'Paraná',
				SC: 'Santa Catarina',
			},
		}]
	}, {
		label: 'templates.formUser.typeAddress',
		isMandatory: true,
		inputs: [{
			type: 'dropdown',
			name: 'typeAddress',
			placeholder: 'templates.formUser.typeAddress.placeholder',
			options: {
				casa: 'Casa',
				empresa: 'Empresa',
			},
		}],
	}, {
		label: 'templates.formUser.address',
		isMandatory: true,
		when: (fields) => fields && fields.typeAddress,
		inputs: [{
			type: 'input',
			placeholder: 'templates.formUser.address.placeholder',
			name: 'address',
		}],
	}, {
		label: 'templates.formUser.interest',
		inputs: [{
			type: 'inputTag',
			name: 'interest',
			placeholder: 'templates.formUser.interest.placeholder',
			enableRemove: true,
		}],
	}, {
		inputs: [{
			type: 'checkbox',
			name: 'receiveEmail',
			label: 'templates.formUser.receiveEmail',
		}],
	}],
};

const FormUser = (props: Props) => {

	const [ fields, setFields ] = useState(props.user || {});

	useEffect(() => {
		setFields(props.user);
	}, [props.user]);

	const onChangeFormHandler = (newFields) => {
		setFields({...fields, ...newFields});
	};

	const onChangePhotoHandler = (photo) => {
		setFields({...fields, photo});
	};

	const onSaveHandler = () => {
		window.localStorage && window.localStorage.setItem('user', JSON.stringify(fields));
		Router.push('/result');
	};

	const isValidAllFields = validateInfo(fields, confForm.fields);
	const hasPhoto = fields ? !!fields.photo : false;

	return (
		<div className={s.root}>
			<InputPhoto value={props.user ? props.user.photo : undefined} onChange={onChangePhotoHandler} label={props.intl.formatMessage({id: 'templates.formUser.upload'})} />
			<Section>
				<Form options={confForm} onChange={onChangeFormHandler} initialValue={props.user} />
				<Button
					disabled={isValidAllFields !== true || !hasPhoto}
					disabledDescription={isValidAllFields !== true ? isValidAllFields : 'templates.formUser.necessaryUploadPhoto'}
					label={props.intl.formatMessage({id: 'templates.formUser.save'})}
					onClick={onSaveHandler}
				/>
			</Section>
		</div>
	);
};

export default injectIntl(FormUser);
