// @flow

export default function (fields: Object, fieldsConfig: Object) {

	let hasSomeFieldMandatory = false;
	let hasSomeFieldInvalid = false;

	fieldsConfig.forEach(field => {

		field.inputs.forEach(input => {
			const value = fields ? fields[input.name] : false;
			const isValid = fields ? fields[`${input.name}.valid`] : true;

			if ((value === '' || !value) && field.isMandatory) hasSomeFieldMandatory = true;
			if (isValid === false) hasSomeFieldInvalid = true;
		});
	});

	if (hasSomeFieldMandatory && hasSomeFieldInvalid) return 'templates.formUser.necessaryFillFieldAndFixInvalid';
	if (hasSomeFieldMandatory) return 'templates.formUser.necessaryFilField';
	if (hasSomeFieldInvalid) return 'templates.formUser.necessaryFixInvalid';

	return true;
}
