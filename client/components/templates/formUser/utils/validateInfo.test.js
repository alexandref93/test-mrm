import validateInfo from './validateInfo';

test('validateInfo', () => {
	const fields = [{
		label: 'templates.formUser.name',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'firstName',
			placeholder: 'templates.formUser.name.firstName.placeholder',
			isValid: (value) => /^\D{0,20}$/g.test(value),
		}, {
			type: 'text',
			name: 'lastName',
			placeholder: 'templates.formUser.name.lastName.placeholder',
			isValid: (value) => /^\D+$/g.test(value),
		}],
	}, {
		label: 'templates.formUser.email',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'email',
			placeholder: 'templates.formUser.email.placeholder',
		}],
	}];

	expect(validateInfo({}, fields)).toBe('templates.formUser.necessaryFilField');
	expect(validateInfo({firstName: '123', 'firstName.valid': false, lastName: 'fuechter'}, fields)).toBe('templates.formUser.necessaryFillFieldAndFixInvalid');
	expect(validateInfo({firstName: '123', 'firstName.valid': false,  lastName: 'fuechter', email: 'teste@teste.com'}, fields)).toBe('templates.formUser.necessaryFixInvalid');
	expect(validateInfo({firstName: 'carlos alexandre', lastName: 'fuechter', email: 'teste@teste.com'}, fields)).toBe(true);
});
