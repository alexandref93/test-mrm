// @flow
import { injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';

import Section from '../../atoms/section';
import Button from '../../atoms/button';
import InputPhoto from '../../molecules/inputPhoto';

import s from './index.mod.css';

type Props = {
  user: Object,
  intl: IntlShape,
};

const ResultUser = (props: Props) => {

	const onChangePhotoHandler = (value) => {

		if (!window.localStorage) return false;

		const user = JSON.parse(window.localStorage.getItem('user'));
		user.photo = value;
		console.log(user);
		window.localStorage.setItem('user', JSON.stringify(user));
	};

	const onClickHandler = () => {
		fetch('/sent', {
			method: 'POST',
			body: JSON.stringify(props.user),
		});
	};

	return (
		<div className={s.root}>
			<sidebar className={s.sidebar}>
				<InputPhoto value={props.user.photo} label={props.intl.formatMessage({id: 'templates.resultUser.changePhoto'})} onChange={onChangePhotoHandler} />
				<Button type='link' label={props.intl.formatMessage({id: 'templates.resultUser.editProfile'})} href='/form' query={{edit: true}} />
				<Button label={props.intl.formatMessage({id: 'templates.resultUser.confirm'})} onClick={onClickHandler}/>
			</sidebar>
			<Section>
				<p className={s.text}>{props.intl.formatMessage({id: 'templates.resultUser.aboutUser'}, {
					name: `${props.user.firstName} ${props.user.lastName}`,
					age: props.intl.formatMessage({id: `templates.resultUser.age.${props.user.age}`}),
					email: props.user.email,
					state: props.user.state,
					interest: props.user.interest,
					receiveEmail: props.user.receiveEmail,
					telephone: props.user.telephone,
				})}</p>
			</Section>
		</div>
	);
};

export default injectIntl(ResultUser);
