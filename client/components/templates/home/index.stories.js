import React from 'react';

import { storiesOf } from '@storybook/react';

import Home from './index';

storiesOf('UI/Templates/Home', module)
	.add('default', () => <Home />);
