import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import Home from './index';

test('snapshot home', () => {

  const wrapper = mount(
    withIntl(
      <Home />
    ),
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});
