// @flow
import { injectIntl } from 'react-intl';

import Button from '../../atoms/button';

import s from './index.mod.css';

const Home = ({ intl }) => {
	return (
		<div className={s.root}>
			<img className={s.logo} src='/static/imgs/logo.png'/>
			<Button type='link' href='/form' label={intl.formatMessage({id: 'call-to-action'})} />
		</div>
	);
};

export default injectIntl(Home);
