// @flow
import { useState, useEffect } from 'react';

import Input from '../../atoms/input';
import Tag from '../../atoms/tag';

import s from './index.mod.css';

type Props = {
  value?: string,
  placeholder: string,
  onChange: (string) => void,
  enableRemove: boolean,
};

export default (props: Props) => {

	const [value, setValue] = useState(props.value || '');
	const tags = value ? value.split(',') : [];

	useEffect(() => {
		// $FlowFixMe
		setValue(props.value);
	}, [props.value]);

	const onChangeHandler = (value) => {
		setValue(value);
		props.onChange && props.onChange(value);
	};

	const onRemoveHandler = (valueRemove) => {
		const tags = value.split(',');
		setValue(tags.filter(tag => tag !== valueRemove).join(','));
		props.onChange && props.onChange(value);
	};

	return (
		<div className={s.root}>
			<Input value={value} placeholder={props.placeholder} onChange={onChangeHandler} />
			{value && value !== '' && (
				<div className={s.tagsWrapper}>
					{tags.map((tag, key) => (
						<Tag data-type='tag' key={key} value={tag} onRemove={props.enableRemove && onRemoveHandler}/>
					))}
				</div>
			)}
		</div>
	);
};
