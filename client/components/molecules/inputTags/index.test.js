import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import Tag from '../../atoms/tag';

import InputTag from './index';

test('snapshot inputTag', () => {

  const wrapper = mount(
    <InputTag value='futebol, video-game, cinema'/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('count tags inputTag', () => {

  const wrapper = mount(
    <InputTag value='futebol, video-game, cinema'/>,
  );

  expect(wrapper.find('[data-type="tag"]').length).toBe(3);
});
