import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import InputTags from './index';

storiesOf('UI/Molecules/InputTags', module)
	.add('default', () => <InputTags />)
	.add('with enableRemove', () => <InputTags enableRemove />);
