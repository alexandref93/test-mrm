import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import Field from './index';

test('snapshot field', () => {

  const wrapper = mount(
    <Field label='label'>
      <h1>Hello World</h1>
    </Field>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('children field', () => {

  const wrapper = mount(
    <Field label='label'>
      <h1>Hello World</h1>
    </Field>,
  );

  expect(wrapper.find('h1').length).toBe(1);
});
