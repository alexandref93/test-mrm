// @flow
import * as React from 'react';

import s from './index.mod.css';

type Props = {
  label: string,
  children: React.Node,
};

export default (props: Props) => {
	return (
		<div className={s.root}>
			<label>{props.label}</label>
			<div className={s.inputs}>
				{props.children}
			</div>
		</div>
	);
};
