import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Input from '../../atoms/input';

import Field from './index';

storiesOf('UI/Molecules/Field', module)
	.add('default', () => (
		<Field label='Name'>
			<Input />
		</Field>
	));
