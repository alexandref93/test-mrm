import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import InputPhoto from './index';

storiesOf('UI/Molecules/InputPhoto', module)
	.add('default', () => <InputPhoto label='Upload a photo' onChange={action('onChange')} />);
