// @flow

type OptionsType = {
  file: any,
};

export default ({ file } : OptionsType = {}) => {
	const fr = new FileReader();
	// $FlowFixMe
	const promise = new Promise(resolve => {
		fr.readAsDataURL(file);
		fr.onload = function (e) {
			resolve(this.result);
		};
	});

	return promise;

};
