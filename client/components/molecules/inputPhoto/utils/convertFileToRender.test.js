import convertFileToRender from './convertFileToRender';

test('convertFileToRender', () => {
	const fileContents = 'file contents';
	const file = new Blob([fileContents], {type : 'text/plain'});

	expect.assertions(1);

	return convertFileToRender({file}).then(data => {
		expect(data !== undefined && data !== null).toBeTruthy();
	});
});
