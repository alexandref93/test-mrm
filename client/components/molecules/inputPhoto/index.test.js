import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import InputPhoto from './index';

test('snapshot inputPhoto', () => {

  const wrapper = mount(
    <InputPhoto label='label'/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onChange inputPhoto', () => {

  jest.useFakeTimers();

  const onChange = sinon.spy();
  const fileContents = 'file contents';
  const file = new Blob([fileContents], {type : 'text/plain'});

  const wrapper = mount(
    <InputPhoto label='label' onChange={onChange}/>,
  );

  wrapper.find('[type="file"]').simulate('change', {
    target: {
      files: [file],
    },
  });

  jest.runAllTimers();

  setTimeout(() => {
    expect(onChange.calledOnce).toBeTruthy();
  }, 500);


});
