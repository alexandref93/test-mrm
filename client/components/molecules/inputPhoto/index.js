// @flow
import { useRef } from 'react';

import Button from '../../atoms/button';
import input from '../../atoms/input';

import { convertFileToRender } from './utils';
import s from './index.mod.css';

type Props = {
  label: string,
  value?: string,
  onChange?: (string) => void,
};

export default (props: Props) => {

	const inputRef = useRef(null);
	const imgRef = useRef(null);

	const onClickHandler = () => {
		inputRef.current && inputRef.current.click();
	};

	const onChangeHandler = (event) => {
		convertFileToRender({
			imgRef,
			file: event.target.files[0],
		}).then(data => {
			// $FlowFixMe
			imgRef.current.src = data;
			props.onChange && props.onChange(data);
		});
	};

	return (
		<div className={s.root}>
			<img className={s.img} ref={imgRef} src={props.value}/>
			<Button onClick={onClickHandler} label={props.label}/>
			<input style={{display: 'none'}} ref={inputRef} onChange={onChangeHandler} type='file'/>
		</div>
	);
};
