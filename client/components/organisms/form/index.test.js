import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import Form from './index';

test('snapshot inputTag', () => {

  const confForm = {
    fields: [{
      label: 'nickname',
      isMandatory: true,
      inputs: [{
        type: 'text',
        name: 'firstName',
        placeholder: 'Your nickname',
        isValid: (value) => /^\D{0,20}$/g.test(value),
      }],
    }, {
      label: 'templates.formUser.email',
      isMandatory: true,
      inputs: [{
        type: 'text',
        name: 'email',
        placeholder: 'Your e-mail',
      }],
    }, {
      label: 'Video-game',
      isMandatory: true,
      inputs: [{
        type: 'dropdown',
        name: 'videogame',
        placeholder: 'Select a favorite',
        options: {
          ps4: 'Playstation 4',
          xbox: 'Xbox One',
        },
      }],
    }, {
      inputs: [{
        type: 'checkbox',
        name: 'wantAVideoGame',
        label: 'I want a video-game',
      }],
    }],
  };

  const wrapper = mount(
    withIntl(
      <Form options={confForm}/>
    ),
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onChange form', () => {

  const confForm = {
    fields: [{
      label: 'nickname',
      isMandatory: true,
      inputs: [{
        type: 'text',
        name: 'firstName',
        placeholder: 'Your nickname',
      }],
    }]
  };

  const onChange = sinon.spy();

  const wrapper = mount(
    withIntl(
      <Form options={confForm} onChange={onChange} />
    ),
  );

  wrapper.find('input').simulate('change', {
    target: {
      value: '123',
    },
  });

  expect(onChange.calledOnce).toBeTruthy();

  expect(onChange.calledWith({
    firstName: '123',
    'firstName.valid': true,
  })).toBeTruthy();

  wrapper.find('input').simulate('change', {
    target: {
      value: 'carlos',
    },
  });

  expect(onChange.calledWith({
    firstName: 'carlos',
    'firstName.valid': true,
  })).toBeTruthy();


});
