// @flow
import React, { useState, useEffect } from 'react';
import { injectIntl } from 'react-intl';
import type { IntlShape } from 'react-intl';

import Field from '../../molecules/field';

import { getTypeInput } from './utils';
import type { InputType } from './utils';

type InputConfType = {
  type: InputType,
  name: string,
  placeholder: string,
  label?: string,
};

type FieldType = {
  label?: string,
  isMandatory: boolean,
  when: (Object) => boolean,
  inputs: Array<InputConfType>,
};

type Props = {
  options: {
    fields: Array<FieldType>,
  },
  onChange?: (Object) => void,
  initialValue: Object,
  intl: IntlShape,
};

const Form = (props: Props) => {

	const [ fields, setFields ] = useState(props.initialValue || {});

	useEffect(() => {
		setFields(props.initialValue);
	}, [props.initialValue]);

	const onChangeHandler = (name, value, isValid) => {
		const newObject = {...fields, [name]: value, [`${name}.valid`]: isValid !== undefined ? isValid : true};
		setFields(newObject);
		props.onChange && props.onChange(newObject);
	};

	return (
		<React.Fragment>
			{props.options.fields.map((field, keyField) => {

				if (field.when && !field.when(fields)) return null;

				return (
					<Field key={keyField} label={(field.label ? props.intl.formatMessage({id: field.label}) : '') + (field.isMandatory ? ' *' : '')}>
						{field.inputs.map((input, key) => {
							const Component = getTypeInput(input.type);
							return <Component {...{
								...input,
								key,
								value: fields ? fields[input.name] : undefined,
								checked: fields ? fields[input.name] : undefined,
								onChange: onChangeHandler.bind(null, input.name),
								placeholder: input.placeholder ? props.intl.formatMessage({id: input.placeholder}) : null,
								label: input.label ? props.intl.formatMessage({id: input.label}) : null,
							}}
							/>;
						})}
					</Field>
				);
			})}
		</React.Fragment>
	);
};

export default injectIntl(Form);
