import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Form from './index';

const confForm = {
	fields: [{
		label: 'nickname',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'firstName',
			placeholder: 'Your nickname',
			isValid: (value) => /^\D{0,20}$/g.test(value),
		}],
	}, {
		label: 'templates.formUser.email',
		isMandatory: true,
		inputs: [{
			type: 'text',
			name: 'email',
			placeholder: 'Your e-mail',
		}],
	}, {
		label: 'Video-game',
		isMandatory: true,
		inputs: [{
			type: 'dropdown',
			name: 'videogame',
			placeholder: 'Select a favorite',
			options: {
				ps4: 'Playstation 4',
				xbox: 'Xbox One',
			},
		}],
	}, {
		inputs: [{
			type: 'checkbox',
			name: 'wantAVideoGame',
			label: 'I want a video-game',
		}],
	}],
};

storiesOf('UI/Organisms/Form', module)
	.add('default', () => <Form options={confForm} onChange={action('onChange')} />);
