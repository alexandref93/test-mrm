import Input from '../../../atoms/input';
import Range from '../../../atoms/range';
import Dropdown from '../../../atoms/dropdown';
import Checkbox from '../../../atoms/checkbox';
import InputTag from '../../../molecules/inputTags';

import getTypeInput from './getTypeInput';

test('getTypeInput', () => {
	expect(getTypeInput('text')).toEqual(Input);
	expect(getTypeInput('range')).toEqual(Range);
	expect(getTypeInput('dropdown')).toEqual(Dropdown);
	expect(getTypeInput('checkbox')).toEqual(Checkbox);
	expect(getTypeInput('inputTag')).toEqual(InputTag);
});
