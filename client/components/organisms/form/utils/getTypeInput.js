// @flow



import Input from '../../../atoms/input';
import Range from '../../../atoms/range';
import Dropdown from '../../../atoms/dropdown';
import Checkbox from '../../../atoms/checkbox';
import InputTag from '../../../molecules/inputTags';

export type InputType = 'range' | 'dropdown' | 'inputTag' | 'checkbox' | 'text';

export default (type : InputType) => {
	switch (type) {
	case 'range':
		return Range;
	case 'dropdown':
		return Dropdown;
	case 'inputTag':
		return InputTag;
	case 'checkbox':
		return Checkbox;
	default:
		return Input;
	}
};
