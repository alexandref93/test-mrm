import type { InputType as InputTypeAlias } from './getTypeInput';

export { default as getTypeInput } from './getTypeInput';
export type InputType = InputTypeAlias;
