import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Checkbox from './index';

storiesOf('UI/Atoms/Checkbox', module)
	.add('default', () => <Checkbox label='Algum checkbox' />)
	.add('with initial value', () => <Checkbox label='Alguma coisa para marcar' checked />);

