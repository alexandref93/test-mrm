// @flow
import { useState, useEffect } from 'react';

import s from './index.mod.css';

type Props = {
  label: string,
  checked: boolean,
  onChange?: (boolean) => void,
};

export default (props: Props) => {

	const [ checked, setCheck ] = useState(props.checked || false);

	useEffect(() => {
		setCheck(props.checked);
	}, [props.checked]);

	const onClickHandler = () => {
		const newValue = !checked;
		setCheck(newValue);
		props.onChange && props.onChange(newValue);
	};

	return (
		<div className={s.root} onClick={onClickHandler}>
			<div className={s.check}>{checked && '✓'}</div>
			<label>{props.label}</label>
		</div>
	);
};
