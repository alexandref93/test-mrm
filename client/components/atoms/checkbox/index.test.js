import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import sinon from 'sinon';

import Checkbox from './index';

test('snapshot checkbox', () => {

  const wrapper = mount(
    <Checkbox label='Checkbox whatever'/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onChange checkbox', () => {

  const onChange = sinon.spy();

  const wrapper = mount(
    <Checkbox label='Checkbox whatever' onChange={onChange} />
  );

  wrapper.simulate('click');

  expect(onChange.calledWith(true)).toBeTruthy();
});

test('onChange checkbox with initial value', () => {

  const onChange = sinon.spy();

  const wrapper = mount(
    <Checkbox label='Checkbox whatever' checked={true} onChange={onChange} />
  );

  wrapper.simulate('click');

  expect(onChange.calledWith(false)).toBeTruthy();
});
