import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import Range from './index';

test('snapshot range', () => {

  const wrapper = mount(
    <Range steps={[{
        value: '123',
        label: '1',
      }, {
        value: '321',
        label: '2',
      }, {
        value: '1234',
        label: '3',
    }]} />
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onChange ranger', () => {

  const onChange = sinon.spy();

  const wrapper = mount(
    <Range onChange={onChange} steps={[{
        value: '123',
        label: '1',
      }, {
        value: '321',
        label: '2',
      }, {
        value: '1234',
        label: '3',
    }]} />
  );

  wrapper.find('[data-id="321"]').simulate('click');

  expect(onChange.calledWith('321')).toBeTruthy();
});

