import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Range from './index';

storiesOf('UI/Atoms/Range', module)
	.add('default', () => <Range onChange={action('onChange')} steps={[{
		value: '123',
		label: '1',
	}, {
		value: '321',
		label: '2',
	}, {
		value: '1234',
		label: '3',
	}]} />)
	.add('with initial value', () => <Range value='321' onChange={action('onChange')} steps={[{
		value: '123',
		label: '1',
	}, {
		value: '321',
		label: '2',
	}, {
		value: '1234',
		label: '3',
	}]} />);
