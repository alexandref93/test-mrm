// @flow

import { useState, useEffect } from 'react';

import s from './index.mod.css';

type StepType = {
  value: string,
  label: string,
};

type Props = {
  onChange?: (string) => void,
  steps: Array<StepType>,
  value?: string,
};

export default (props: Props) => {

	const [ stepSelected, setStep ] = useState(props.value || props.steps[0].value);

	useEffect(() => {
		setStep(props.value);
	}, [props.value]);

	const onClickHandler = (value) => {
		setStep(value);
		props.onChange && props.onChange(value);
	};

	return (
		<div className={s.root}>
			{props.steps.map((step, key) => (
				<div data-id={`${step.value}`} key={key} className={s.step} onClick={() => onClickHandler(step.value)}>
					{stepSelected === step.value && <span className={s.stepSelected} />}
					<div className={s.stepLabel}>{step.label}</div>
				</div>
			))}
		</div>
	);
};
