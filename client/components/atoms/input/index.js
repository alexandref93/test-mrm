// @flow
import classnames from 'classnames';

import s from './index.mod.css';

type Props = {
  placeholder?: string,
  value?: string,
  onChange?: (string, boolean) => void,
  isValid?: (string) => boolean,
};

export default (props: Props) => {

	const validField = (value) => {
		return props.isValid && props.value && props.value !== '' ? props.isValid(props.value) : true;
	};

	const onChangeHandler = (event) => {
		// $FlowFixMe
		props.onChange && props.onChange(event.target.value, validField(event.target.value));
	};

	const isValid = validField(props.value);

	return (
		<div className={classnames(s.root, {[s.isInvalid]: !isValid})}>
			<input className={s.input} placeholder={props.placeholder} value={props.value} onChange={onChangeHandler} />
			{!isValid && (
				<span className={s.iconInvalid}>
          ✖
				</span>
			)}
		</div>
	);
};
