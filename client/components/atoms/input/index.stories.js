import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Input from './index';

storiesOf('UI/Atoms/Input', module)
	.add('default', () => <Input placeholder="Some placeholder" />)
	.add('with isValid valid', () => <Input value="hello world" placeholder="Some placeholder" isValid={(value) => /^\D+$/g.test(value)} />)
	.add('with isValid invalid', () => <Input value="123" placeholder="Some placeholder" isValid={(value) => /^\D+$/g.test(value)} />);
