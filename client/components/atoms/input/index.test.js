import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import Input from './index';

test('snapshot input', () => {

  const wrapper = mount(
    <Input placeholder='Input whatever' value='Test legal'/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onChange input', () => {
  const event = {target: {value: 'any value'}};
  const onChange = sinon.spy();
  const wrapper = mount(<Input placeholder='Input whatever' value='Test legal' onChange={onChange}/>);

  wrapper.find('input').simulate('change', event);

  expect(onChange.calledWith('any value')).toBeTruthy();
});

test('isValid input', () => {

  const wrapper = mount(<Input placeholder='Input whatever' value='carlos alexandre' isValid={(value) => /^\D+$/g.test(value)}/>);

  expect(wrapper.find('span').length).toBe(0);
});

test('isValid input invalid', () => {

  const wrapper = mount(<Input placeholder='Input whatever' value='123123123123' isValid={(value) => /^\D+$/g.test(value)}/>);

  expect(wrapper.find('span').length).toBe(1);
});


