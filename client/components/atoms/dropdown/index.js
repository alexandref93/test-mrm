// @flow

import s from './index.mod.css';

type Props = {
  placeholder?: string,
  value?: string,
  onChange?: (string) => void,
  options: {
    [string]: string,
  },
};

export default (props: Props) => {

	const onClickHandler = (value) => {
		props.onChange && props.onChange(value);
	};

	return (
		<div className={s.root}>
			<div className={s.header}>
				{props.value ? props.options[props.value] : <span className={s.placeholder}>{props.placeholder}</span>}
				<span className={s.icon}>
          ▼
				</span>
			</div>
			<ul className={s.list}>
				{Object.keys(props.options).map(keyOption => (
					<li key={keyOption} className={s.item} onClick={() => onClickHandler(keyOption)}>{props.options[keyOption]}</li>
				))}
			</ul>
		</div>
	);
};
