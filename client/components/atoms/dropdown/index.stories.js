import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Dropdown from './index';

storiesOf('UI/Atoms/Dropdown', module)
	.add('default', () => (
		<Dropdown options={{
			casa: 'casa',
			trabalho: 'trabalho',
			lazer: 'lazer',
		}} />
	))
	.add('with placeholder', () => (
		<Dropdown placeholder='Escolha uma opção' options={{
			casa: 'casa',
			trabalho: 'trabalho',
			lazer: 'lazer',
		}} />
	));
