import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import Dropdown from './index';

test('snapshot dropdown', () => {

  const wrapper = mount(
    <Dropdown placeholder='Dropdown whatever' options={{
      casa: 'casa',
      trabalho: 'trabalho',
      lazer: 'lazer',
    }}/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});
