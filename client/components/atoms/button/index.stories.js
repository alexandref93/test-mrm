import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from './index';

storiesOf('UI/Atoms/Button', module)
	.add('default', () => <Button label='Button whatever' onClick={action('onClick')} />)
	.add('with disabled true', () => <Button label='Button whatever' onClick={action('onClick')} disabled />);
