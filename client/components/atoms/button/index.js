// @flow
import { useState } from 'react';
import Link from 'next/link';
import classnames from 'classnames';

import s from './index.mod.css';

type Props = {
  type: 'button' | 'link',
  label: string,
  onClick?: () => void,
  href?: string,
  query?: Object,
  className?: string,
  disabled: boolean,
  disabledDescription?: string,
};

export default (props: Props) => {

	if (props.type === 'link') return (
		<Link href={{pathname: props.href, query: props.query}}>
			<button className={classnames(s.button, props.className)}>{props.label}</button>
		</Link>
	);

	const [ showDisabledDescription, setDisableDescription ] = useState(false);

	const onClickHandler = () => {
		if (props.disabled) {
			setDisableDescription(true);

			setTimeout(() => {
				setDisableDescription(false);
			}, 600 * 10);
			return;
		}

		props.onClick && props.onClick();
	};

	return (
		<span className={s.root}>
			<button className={classnames(s.button, props.className, {[s.disabled]: props.disabled})} onClick={onClickHandler}>{props.label}</button>
			{showDisabledDescription && <p>{props.disabledDescription}</p>}
		</span>
	);
};
