import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import sinon from 'sinon';

import Button from './index';

test('snapshot button', () => {

  const wrapper = mount(
    <Button label='Button whatever'/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onClick button', () => {

  const onClick = sinon.spy();

  const wrapper = mount(<Button label='save' onClick={onClick} />);

  expect(onClick.calledOnce).toBeFalsy();

  wrapper.find('button').simulate('click');

  expect(onClick.calledOnce).toBeTruthy();
});

test('disabled button', () => {

  const onClick = sinon.spy();

  const wrapper = mount(<Button disabled label='save' onClick={onClick} />);

  expect(onClick.calledOnce).toBeFalsy();

  wrapper.find('button').simulate('click');

  expect(onClick.calledOnce).toBeFalsy();
});
