import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Tag from './index';

storiesOf('UI/Atoms/Tag', module)
	.add('default', () => <Tag value='Programação' />)
	.add('with onRemove', () => <Tag value='JavaScript' onRemove={action('onRemove')} />);
