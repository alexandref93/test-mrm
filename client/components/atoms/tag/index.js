// @flow

import s from './index.mod.css';

type Props = {
  value: string,
  onRemove: (string) => void,
};

export default (props: Props) => {

	const onClickRemoveHandler = (value) => {
		props.onRemove && props.onRemove(value);
	};

	return <span className={s.root}>
		{props.value} {props.onRemove && <span data-id='remove' className={s.iconRemove} onClick={onClickRemoveHandler.bind(null, props.value)}>✕</span>}
	</span>;
};
