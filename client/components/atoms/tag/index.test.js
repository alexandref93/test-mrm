import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';
import toJson from 'enzyme-to-json';

import Tag from './index';

test('snapshot tag', () => {

  const wrapper = mount(
    <Tag value='javascript'/>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('onChange tag', () => {

  const onRemove = sinon.spy();

  const wrapper = mount(
    <Tag value='javascript' onRemove={onRemove}/>,
  );

  expect(onRemove.calledOnce).toBeFalsy();

  wrapper.find('[data-id="remove"]').simulate('click');

  expect(onRemove.calledWith('javascript')).toBeTruthy();
});
