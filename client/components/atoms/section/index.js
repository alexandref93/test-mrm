// @flow
import * as React from 'react';

import s from './index.mod.css';

type Props = {
  children: React.Node,
};

export default (props: Props) => {
	return (
		<div className={s.root}>
			{props.children}
		</div>
	);
};
