import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Section from './index';

storiesOf('UI/Atoms/Section', module)
	.add('default', () => <Section><h1>Hello World</h1></Section>);
