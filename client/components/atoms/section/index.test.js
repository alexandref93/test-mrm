import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import Section from './index';

test('snapshot section', () => {

  const wrapper = mount(
    <Section><h1>Hello World</h1></Section>,
  );

  expect(toJson(wrapper)).toMatchSnapshot();
});

test('has children section', () => {

  const wrapper = mount(
    <Section><h1>Hello World</h1></Section>,
  );

  expect(wrapper.find('h1').length).toBe(1);
});
