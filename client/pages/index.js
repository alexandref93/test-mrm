// @flow

import React from 'react';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';

import Home from '../components/templates/home';

const PageHome = ({ intl }) => {
	return (
		<React.Fragment>
			<Helmet>
				<title>{intl.formatMessage({id: 'title'})}</title>
			</Helmet>
			<Home />
		</React.Fragment>
	);
};

export default injectIntl(PageHome);
