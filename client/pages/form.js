// @flow

import React, { useEffect, useState } from 'react';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';

import FormUser from '../components/templates/formUser';

const PageForm = ({ intl, edit }) => {

	const [ user, setUser ] = useState({});

	useEffect(() => {
		setUser(window && window.localStorage ? JSON.parse(window.localStorage.getItem('user')) : {});
	}, []);

	return (
		<React.Fragment>
			<Helmet>
				<title>{intl.formatMessage({id: 'pages.form.title'})}</title>
			</Helmet>
			<FormUser user={edit ? user : undefined} />
		</React.Fragment>
	);
};

PageForm.getInitialProps = async ({ query }) => {
	return {
		edit: query.edit || false,
	};
};

export default injectIntl(PageForm);
