// @flow

import React, { useEffect, useState } from 'react';
import Helmet from 'react-helmet';
import { injectIntl } from 'react-intl';

import ResultUser from '../components/templates/resultUser';

const PageResult = ({ intl }) => {

	const [ user, setUser ] = useState({});

	useEffect(() => {
		setUser(window && window.localStorage ? JSON.parse(window.localStorage.getItem('user')) : {});
	}, []);

	return (
		<React.Fragment>
			<Helmet>
				<title>{intl.formatMessage({id: 'pages.result.title'})}</title>
			</Helmet>
			<ResultUser user={user} />
		</React.Fragment>
	);
};

export default injectIntl(PageResult);
