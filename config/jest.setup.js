import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { IntlProvider } from 'react-intl';

import locales from '../lang/en-US.json';

configure({ adapter: new Adapter() });

global.withIntl = element => (
  <IntlProvider locale='en-US' messages={locales}>
     { element }
  </IntlProvider>
)
