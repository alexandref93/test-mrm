const withCSS = require('@zeit/next-css')

const configPostCSS = require('./config/postcss.config');

module.exports = withCSS({
  ...configPostCSS,
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  }
})
