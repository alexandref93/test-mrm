# Test Mirum

## Tópicos

- Como rodar o projeto
- Como rodar os testes
- Tecnologias usadas
- Sobre mim

## Como rodar o projeto

Versão mínima do [node.js](https://nodejs.org/en/): `v10.14.2`

Executar os comandos abaixo na pasta raíz:

```
npm install
npm run dev
```

Para acessar o storybook (para ver os componentes catalogado) do projeto basta rodar o comando:

```npm run storybook```

**obs**: Isso depois que instalou todos os pacotes do projeto.

## Como rodas os testes

```
npm run test
```

## Tecnologias usadas

- [next.js](https://github.com/zeit/next.js/)
- [react](https://reactjs.org/) (com hooks)
- [flow](https://flow.org/) (tipagem)
- [eslint](https://eslint.org/) (qualidade do código)
- [jest](https://jestjs.io/) (test unit)
- [storybook](https://github.com/storybooks/storybook) (catalogo dos componentes)

## Sobre mim

[LinkedIn](https://www.linkedin.com/in/fuechter/)

*email:* fuechter.carlos@gmail.com

Meu nome é Carlos, tenho 8 anos de profissão com desenvolvimento de software, dos quais desenvolvi experiência desde ERP (Gestão Empresarial), E-commerce, App Mobiles (o mais recente foi para a Gol companhia aérea brasileira) até plataforma de compras colaborativas, passando por diversas etapas de projeto desde back-end até front-end. Busco projetar uma melhor arquitetura para manter o projeto ao longo-prazo e saudável para os negócios. Percebi que ao longo da minha carreira, sempre me vi como um apaixonado por novas tecnologias, sempre em busca de otimização do trabalho e novos conceitos de abordar determinados problemas.

Nos últimos 3 anos tenho trabalhado para a startup Comida Da Gente, uma plataforma que permite compras colaborativas, conectando produtores locais de comida orgânica ao consumidor final, facilitando o processo de acesso a produto de qualidade com preço acessível, quanto o produtor consegue otimizar a sua  logística de entrega. Um cenário de diversos desafios, que permite uma melhor entrega de dados para o usuário que possui uma internet de baixa qualidade, até os perfis de usuários que têm pouca familiaridade com o mundo digital. 

Participei ativamente como desenvolvedor front-end sênior trabalhando remotamente, usando tecnologias como React, Redux, Relay, GraphQL, Nodejs e Jest para testes unitários de todos os componentes e funções. Trabalhando com modelo de arquitetura "atomic" de componentes para atender a demanda de que a plataforma tende a ser estruturalmente grande, e manter mais simplicidade na hierarquia dos componentes, além de tentar prezar por uma abordagem de programação funcional.
