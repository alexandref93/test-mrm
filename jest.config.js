module.exports = {
  setupFiles: ['<rootDir>/config/jest.setup.js'],
  testPathIgnorePatterns: ['<rootDir>/client/.next/', '<rootDir>/node_modules/'],
  testURL: 'http://localhost:3000',
  verbose: true,
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/test/styleOrFile.js',
    '\\.(css|less)$': '<rootDir>/test/styleOrFile.js',
  },
}
