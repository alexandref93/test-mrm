const path = require('path');

const loaderPostCSS = {
  loader: 'postcss-loader',
  options: {
    config: {
      path: path.resolve(__dirname, '../config/postcss.config.js'),
    },
  },
};

const loaderCssMod = {
  loader: 'css-loader',
  options: {
    modules: true,
    localIdentName: '[name]__[local]--[hash:base64:5]',
  },
};

module.exports = (baseConfig, env, defaultConfig) => {

  defaultConfig.module.rules = defaultConfig.module.rules.filter(rule => !rule.use || (rule.use && !rule.use.some(use => use.indexOf && use.indexOf('style-loader') > -1)));

  defaultConfig.module.rules.push({
    test: /\.mod\.css$/,
    use:  ['style-loader', loaderCssMod, loaderPostCSS],
  });

  defaultConfig.resolve.alias['react-dom'] = '@hot-loader/react-dom';
  return defaultConfig;
};
