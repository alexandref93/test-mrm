import { configure, addDecorator } from '@storybook/react';
import { setConfig } from 'react-hot-loader';
import { addLocaleData, IntlProvider } from 'react-intl';

import locales from '../lang/en-US.json';

import '../client/style/style.mod.css';

addLocaleData(locales);

addDecorator(story => (
  <IntlProvider locale='en-US' messages={ locales }>
     { story() }
  </IntlProvider>
))

// automatically import all files ending in *.stories.js
const req = require.context('../client', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

setConfig({ pureSFC: true });

configure(loadStories, module);

